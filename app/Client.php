<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['id','name','identification','phonePrimary','address','city','id_alegra'];
}
