<?php
namespace App\ConsumeApi;

class AlegraApi{

    private $autorizacion = "";
    private $info = array();

    public function __construct()
    {
        $this->autorizacion = config('app.autorizacion_alegra');
    }

    public function postRequest($uri,$data)
    {
        try{
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST',$uri , [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept'     => 'application/json',
                    'Authorization'  => $this->autorizacion
                ],
                'body' => $data
            ]);

            $response = $response->getBody()->getContents();
            $obj=json_decode($response);

            $this->info['state'] = true;
            $this->info['data'] = $obj->{'id'};
            $this->info['message'] = '';
            $this->info['code'] = '';
        }catch (\GuzzleHttp\Exception\RequestException $e){
            $this->info['state'] = false;
            $this->info['data'] = '';
            $this->info['message'] = 'Error: Have a error in server. Try to later.';
            $this->info['code'] = '500';
            if ($e->hasResponse()) {
                $this->info['code'] = $e->getCode();
                if($e->getCode()==400)
                    $this->info['message'] = 'Error: You must review the information provided, it is incomplete or erroneous.';
            }
        }
        return $this->info;
    }

    public function getRequest($uri){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $uri, [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => $this->autorizacion
                ]
            ]);

            $response = $response->getBody()->getContents();
            $obj = json_decode($response);
            return $obj;
        }catch (\GuzzleHttp\Exception\RequestException $e){
            $this->info['state'] = false;
            $this->info['data'] = '';
            $this->info['message'] = 'Error: Have a error in server. Try to later.';
            $this->info['code'] = '500';
            if ($e->hasResponse()) {
                $this->info['code'] = $e->getCode();
                if($e->getCode()==400)
                    $this->info['message'] = 'Error: You must review the information provided, it is incomplete or erroneous.';
            }
        }
    }
}
