<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UnidadVenta;
use Validator;

class UnidadVentaController extends Controller
{
    public function index(){
        return response()->json(['data' => UnidadVenta::all()],200);
    }

    public function store(Request $request){
        $validator = Validator::make(
            $request->all(),[
                'unidad' => 'required',
            ]
        );

        if($validator->fails())
            return response()->json(['error'=>$validator->errors()],400);

        $unidad = new UnidadVenta();
        $unidad->unidad = $request->unidad;

        if($unidad->save())
            return response()->json(['data' => $unidad->toArray(),'state' => true], 201);
        else
            return response()->json(['data' => 'Error: Have a error in server. Try to later.','state' => false], 500);
    }
}
