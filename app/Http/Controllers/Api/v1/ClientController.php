<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Client;
use Validator;
use App\ConsumeApi\AlegraApi;

class ClientController extends Controller
{
    public function index(){
        //return response()->json(['data' => Client::all()],200);
        $alegra = new AlegraApi();
        $uri = 'https://app.alegra.com/api/v1/contacts';
        $info = $alegra->getRequest($uri);
        //print_r($info[0]->{'id'});
        print_r($info);
    }

    public function getClientByName($name){
        $alegra = new AlegraApi();
        $uri = 'https://app.alegra.com/api/v1/contacts/?name='. $name;
        $info = $alegra->getRequest($uri);
        print_r($info);
    }

    public function getClientByIdentification($identification){
        $alegra = new AlegraApi();
        $uri = 'https://app.alegra.com/api/v1/contacts/?identification='. $identification;
        $info = $alegra->getRequest($uri);
        print_r($info);
    }

    public function store(Request $request){
        $validator = Validator::make(
            $request->all(),[
                'name' => 'required',
                'identification' => 'required',
                'phonePrimary' => 'required',
                'address' => 'required',
                'city' => 'required'
            ]
        );

        if($validator->fails())
            return response()->json(['error'=>$validator->errors()],400);

        $alegra = new AlegraApi();
        $uri = 'https://app.alegra.com/api/v1/contacts';
        $data = array(
            'name' => $request->name,
            'identification' => $request->identification,
            'phonePrimary' => $request->phonePrimary,
            'address' => $request->address,
            'city' => $request->city
        );
        $params = json_encode($data);
        $info = $alegra->postRequest($uri,$params);
        if($info['state']==true) {
            return response()->json(['data' => $info,'state' => true], 201);
        }else{
            return response()->json(['data' => $info['message'],'state' => false], $info['code']);
        }

    }
}
