<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Producto;
use Validator;
use App\ConsumeApi\AlegraApi;

class ProductoController extends Controller
{
    public function index(){
        //return response()->json(['data' => Client::all()],200);
        $alegra = new AlegraApi();
        $uri = 'https://app.alegra.com/api/v1/items';
        $info = $alegra->getRequest($uri);
        //print_r($info[0]->{'id'});
        print_r($info);
    }

    public function getProductByQuery($criteria){
        $alegra = new AlegraApi();
        $uri = 'https://app.alegra.com/api/v1/items/?query='. $criteria;
        $info = $alegra->getRequest($uri);
        print_r($info);
    }

    public function store(Request $request){
        $validator = Validator::make(
            $request->all(),[
                'name' => 'required',
                'description' => 'required',
                'price' => 'required',
            ]
        );

        if($validator->fails())
            return response()->json(['error'=>$validator->errors()],400);

        $alegra = new AlegraApi();
        $uri = 'https://app.alegra.com/api/v1/items';
        $data = array(
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price
        );
        $params = json_encode($data);
        $info = $alegra->postRequest($uri,$params);
        if($info['state']==true) {
            $id = $info['data'];
            $product = new Producto();
            $product->id_alegra=$id;
            $product->id_unidadVenta = $request->id_unidadVenta;
            $product->save();
            return response()->json(['data' => $info,'state' => true], 201);
        }else{
            return response()->json(['data' => $info['message'],'state' => false], $info['code']);
        }
    }
}
