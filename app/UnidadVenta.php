<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadVenta extends Model
{
    protected $fillable = ['unidad'];
}
