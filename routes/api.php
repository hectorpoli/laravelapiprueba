<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('client','Api\v1\ClientController@index');
Route::get('client/{name}','Api\v1\ClientController@getClientByName')->where('name', '[A-Za-z]+');
Route::get('client/{identification}','Api\v1\ClientController@getClientByIdentification')->where('id', '[0-9]+');
Route::post('client','Api\v1\ClientController@store');

Route::get('product','Api\v1\ProductoController@index');
Route::get('product/{criteria}','Api\v1\ProductoController@getProductByQuery');
Route::post('product','Api\v1\ProductoController@store');

Route::get('unidadVenta','Api\v1\UnidadVentaController@index');
Route::post('unidadVenta','Api\v1\UnidadVentaController@store');
